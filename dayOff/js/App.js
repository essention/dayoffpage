"use strict";
//<!--STATUS WORKFLOW APPROVAL-->

//NotStarted = 0
//FailedOnStart = 1
//InProgress = 2
//ErrorOccurred = 3
//StoppedByUser = 4
//Completed = 5
//FailedOnStartRetrying = 6
//ErrorOccurredRetrying = 7
//ViewQueryOverflow = 8
//Canceled = 15
//Approved = 16
//Rejected = 17

//const
(function($) {
var MSEC_PER_DAY = 1000*60*60*24;
var DAYS_PER_YEAR = 365;
var VACATION_PER_YEAR = 20;
var loggedUser = {};
loggedUser.userId = _spPageContextInfo.userId;
var holidays_list; 
var holidays_array = [];
var holidays_array_UA = [];
var holidays_array_US = [];
var holidays_array_dates_UA = [];
var holidays_array_dates_US = [];
var promise;
var holiday_array_user;
var holiday_array_dates_user;
var custom_case_for_Admin = true;
var custom_case_Lana_admin = {
    ess_group_admin_id: 18, // to do - find by name
    lana_employee_id: 32
}
var admin_users_ids = [ // temp, need some table list
    18,//Ruslana as Ess Group Admin
    32,//Ruslana
    11,//Chris
    15,//Alex
    49,//Oleksii
    24//Olena
]
loggedUser.displayOtherStats = admin_users_ids.includes(loggedUser.userId);
console.log("loggedUser.displayOtherStats", loggedUser.displayOtherStats);


if (custom_case_for_Admin) {
   if (loggedUser.userId == custom_case_Lana_admin.ess_group_admin_id) {
       loggedUser.userId = custom_case_Lana_admin.lana_employee_id;
   }
}

function ListAllHolidays(vc) {
    //1.
    var wk = this;
    wk.vc = vc;
    wk.executor = new SP.RequestExecutor(wk.vc.appweburl);
    //2.
    // Function to handle the success event on weekend list.  Prints the data to the page.
    wk.successWeekendHandler=function (data) {
        var jsonObject = JSON.parse(data.body);
        var weekendTable = document.getElementById("weekend"); //todo setup selector

        var results = jsonObject.d.results;
        wk.vc.weekendList = results;

        for (var i = 0; i < results.length; i++) {

            var row = weekendTable.insertRow();
            row.className = "tr-user";
            var cellTitle = row.insertCell();
            var cellWeekendDay = row.insertCell();
			var cellUA = row.insertCell();
			var cellUSA = row.insertCell();
			
			cellTitle.innerHTML = results[i].Title;

            var weekTempDay = new Date(results[i][wk.vc.weekend_WeekendDay]);
            var temp = weekTempDay.format('MMM dd yyyy');

            cellWeekendDay.innerHTML = temp;
			
			if(results[i][wk.vc.weekend_UA])
				cellUA.innerHTML = results[i][wk.vc.weekend_UA];
			if(results[i][wk.vc.weekend_USA])
				cellUSA.innerHTML = results[i][wk.vc.weekend_USA];
            
        }
    }
    // Function to handle the error event on weekend list. Prints the error message to the page.
    wk.errorWeekendHandler=function (data, errorCode, errorMessage) {
        document.getElementById("renderAnnouncements").innerText =
            "Could not complete call to 'Weekend' list: " + errorMessage;
    }

    //3.
    wk.executor.executeAsync({
            url:
                wk.vc.appweburl +
                "/_api/web/lists/getbytitle('" + wk.vc.weekend_list_name + "')/items",
            method: "GET",
            headers: { "Accept": "application/json; odata=verbose" },
            success: wk.successWeekendHandler,
            error: wk.errorWeekendHandler
    });

}

function Holidays(vc) {
    //1.
    var h = this;
    h.vc = vc;
    h.executor = new SP.RequestExecutor(h.vc.appweburl);
    console.log("_spPageContextInfo.userDisplayName", _spPageContextInfo.userDisplayName);
    var userName = custom_case_for_Admin ? loggedUser.Title : _spPageContextInfo.userDisplayName; // to do - use in other places 
    h.viewXml = "<View><Query><Where><Eq><FieldRef Name='Employee'/><Value Type='User'>"
        + userName +
        "</Value></Eq></Where></Query></View>";
    h.queryPayload = {
        'query': {
            '__metadata': { 'type': 'SP.CamlQuery' },
            'ViewXml': h.viewXml
        }
    };
    h.d = $.Deferred();

 
    //2.
    h.onQuerySucceededHoliday = function (datalist) {
        
        var vacation_requested = 0; // to do 
        var unpaid_days_off = 0;
        var sick_days = 0;
        
//        console.log("datalist", datalist);
//        getUpcomingVacation(datalist);

        datalist.forEach(function (item, i, datalist) {

            //var workflowStatus = datalist[i].VacationStatus;

            
            var holidayType = datalist[i].TypeOfId;
            
            var timePeriod_start_UTC = new Date(datalist[i][h.vc.holiday.StartDate]).getTime();
            var timePeriod_end_UTC = new Date(datalist[i][h.vc.holiday.EndDate]).getTime();
            
//            console.log("timePeriod_start_", timePeriod_start_UTC, datalist[i][h.vc.holiday.StartDate]);
//            console.log("timePeriod_end_", timePeriod_end_UTC, datalist[i][h.vc.holiday.EndDate]);
            
            var timePeriod_duration = getDurationInDays(datalist[i][h.vc.holiday.StartDate], datalist[i][h.vc.holiday.EndDate]);
            var weekend_days = calcWeekendDaysInPeriod(datalist[i][h.vc.holiday.StartDate], datalist[i][h.vc.holiday.EndDate]);
            
            
//            console.log("holiday_array_user", holiday_array_user);
            var holidays_in_period = calcHolidaysInPeriod(datalist[i][h.vc.holiday.StartDate], datalist[i][h.vc.holiday.EndDate], holiday_array_dates_user);
            
//            console.log("holidays_in_period", holidays_in_period);
 
            var pureVacationTime = timePeriod_duration - weekend_days - holidays_in_period;           
            
            switch (holidayType) {// 1 - vacation, 2 - compensatory leave, 3 - sick leave
                case 1: //paid leave
                    vacation_requested += pureVacationTime;   
                    h.vc.spentHolidays += pureVacationTime;
                    h.vc.freeHolidays -= pureVacationTime; 
                    break;
                case 2: //unpaid leave
                    h.vc.compensatoryHolidays += pureVacationTime; 
                    break;
                case 3: //sick days
                    h.vc.sickDay  += pureVacationTime; 
                    break;
            }
                


        })

        var freeday = document.getElementById('free-days');
        freeday.innerHTML = h.vc.freeHolidays;

        var sick = document.getElementById('sickDays');
        sick.innerHTML = h.vc.sickDay;

        var spent = document.getElementById('spent-holidays');
        spent.innerHTML = h.vc.spentHolidays;

        var compensatory = document.getElementById('compensatory');
        compensatory.innerHTML = h.vc.compensatoryHolidays;
    }

    // Function to handle the success event on weekend list.  Prints the data to the page.
    h.successGetHolidaysHandler = function (data) {
        var datalist = null;
        if (data.body != null) {
            datalist = JSON.parse(data.body).d.results;
        }
        h.onQuerySucceededHoliday(datalist);
    }
    // Function to handle the error event on weekend list. Prints the error message to the page.
    h.errorGetHolidaysHandler = function (data, errorCode, errorMessage) {
        document.getElementById("renderAnnouncements").innerText =
            "Could not complete call to 'Weekend' list: " + errorMessage;
    }

    //3.
    h.executor.executeAsync({
        url: h.vc.appweburl + "/_api/web/lists/GetByTitle('" + h.vc.holiday.holiday_list_name + "')/getitems",
        method: "POST",
        body: JSON.stringify(h.queryPayload),
        headers: {
            "Accept": "application/json; odata=verbose",
            "content-type": "application/json; odata=verbose"
        },
        success: h.successGetHolidaysHandler,
        error: h.errorGetHolidaysHandler
    });

}



function getListItemsFromSPList() {
    var siteUrl = _spPageContextInfo.webAbsoluteUrl;

    promise = $.ajax({
        url: siteUrl + "/_api/web/lists/getbytitle('Holidays list')/items",
        method: "GET",
        headers: { "Accept": "application/json; odata=verbose" },
        success: function (data) {
            // Returning the results
            var listItems = data.d.results;
            console.log("listItems", listItems);
            return listItems;
        },
        error: function (data) {
            console.error("Error: " + data)
        }
    }).then(function(result) {
        console.log("then function", result, holidays_list);
        var holidays_list_len = result.d.results.length;
        for (var i=0; i<holidays_list_len;i++) {
            var holiday = result.d.results[i];
//            console.log("holiday", holiday);
            if (holiday.UA == true) {
                holidays_array_dates_UA.push(holiday.DateofHoliday);
                holidays_array_UA.push(holiday);
            }
            if (holiday.USA == true) {                
                holidays_array_dates_US.push(holiday.DateofHoliday);  
                holidays_array_US.push(holiday);          
            }
        };
        
    });
}
holidays_list = getListItemsFromSPList();

function getUserById(employeeID) {
    var siteUrl = _spPageContextInfo.webAbsoluteUrl;

    promise = $.ajax({
        url: siteUrl + "/_api/web/getuserbyid(" + employeeID + ")",
        method: "GET",
        headers: { "Accept": "application/json; odata=verbose" },
        success: function (data) {
            // Returning the results
            var user24 = data.d;
            console.log("user" + employeeID, user24);
            loggedUser.Title = user24.Title;
            $("#hello-message").append(loggedUser.Title + '!');
//            getUserById(loggedUser.EmployeeId);
            return user24;
        },
        error: function (data) {
            console.error("Error: ", data)
        }
    })
}


function getUserContractById(userId) {
    var siteUrl = _spPageContextInfo.webAbsoluteUrl;

    promise = $.ajax({
        url: siteUrl + "/_api/web/lists/getbytitle('Employee')/Items?$filter=EmployeeId eq " + userId,
        method: "POST",
        headers: { "Accept": "application/json; odata=verbose" },
        success: function (data) {
            // Returning the results
            console.log("data Employee", data);
        },
        error: function (data) {
            console.error("Error: ", data)
        }
    })
}
getUserContractById(loggedUser.userId);
//getUserById(loggedUser.EmployeeId);
//getUserById(28);
//getUserById(20);


function Users(vc) {
    //1.
    var u = this;
    u.vc = vc;
    u.executor = new SP.RequestExecutor(u.vc.appweburl);            

    // Function to handle the success event on weekend list.  Prints the data to the page.
    u.successGetUsersHandler = function (data) {
        var jsonObject = JSON.parse(data.body);
        console.log("jsonObject", jsonObject);
        var results = jsonObject.d.results;
        for (var i = 0; i < results.length; i++) {

            var EmployeeId = results[i].EmployeeId;
            console.log("results[i] / Title / EmployeeId", results[i].Title, EmployeeId);
            if (EmployeeId === loggedUser.userId
                    && (results[i].contractcompleted == null || new Date(results[i].contractcompleted) >= new Date())) {
                vc.userAppRules = results[i].RulesVacationsApp;
                u.getUserApplicationRules(vc.userAppRules); 
                
                
                console.log("results[i]", results[i]);
                loggedUser.Office = results[i].Office;
                holiday_array_user = (loggedUser.Office == 'UA') ? holidays_array_UA : holidays_array_US;
                holiday_array_dates_user = (loggedUser.Office == 'UA') ? holidays_array_dates_UA : holidays_array_dates_US;
//                getUpcomingHolidays(holiday_array_user);
                loggedUser.EmployeeId = results[i].EmployeeId;
                var today_UTC = new Date().getTime();
                var contract_start_UTC = new Date(results[i].Startworkdate).getTime();
                
                console.log("today_UTC", today_UTC);
                console.log("contract_start_UTC", contract_start_UTC);
               
                var contract_duration_by_now = getDurationInDays(contract_start_UTC, today_UTC);//days
                console.log("contract_duration_by_now", contract_duration_by_now);
                var days = Math.ceil((contract_duration_by_now / DAYS_PER_YEAR) * VACATION_PER_YEAR);

                u.vc.freeHolidays = days;
                console.log("days", days);
                var freeday = document.getElementById('free-days');
                freeday.innerHTML = freeday.innerHTML + u.vc.freeHolidays;
                
//                return false; // only one user corresponding
            }
        }
    }
    // Function to handle the error event on weekend list. Prints the error message to the page.
    u.errorGetUsersHandler = function (data, errorCode, errorMessage) {
        document.getElementById("renderAnnouncements").innerText =
         "Could not complete call to 'Users' list: " + errorMessage;
    }

    // Function to get current user's role
    u.getUserApplicationRules = function (ruleName) {
        console.log("getUserApplicationRules", ruleName );
        if (ruleName === "Administrator") {
            $("#holtypes").show();
            $("#essnusers").show();
            $("#weekends").show();
            $("#wrkflowhistory").show();
            $("#wrkflowtask").show();
            $("#admin-table").show();
        }
        return;
    }

    //3.
    u.executor.executeAsync({
           url:
               u.vc.appweburl +
               "/_api/web/lists/getbytitle('" + u.vc.users_list_name + "')/items",
           method: "GET",
           headers: { "Accept": "application/json; odata=verbose" },
           success: u.successGetUsersHandler,
           error: u.errorGetUsersHandler
    });

}

function getUpcomingVacation(vacations_array) {
    var vacation_filtered = upcomingDatesSorted(vacations_array, 'StartDate');
    if (vacation_filtered.length) {
        var start_date = new Date(vacation_filtered[0].StartDate).toLocaleDateString();
        var end_date = new Date(vacation_filtered[0].End_Date).toLocaleDateString();
        var vacation_title = vacation_filtered[0].Title;
        $("section#upcoming-vacation.hide-section").removeClass("hide-section");
        var next_vacation_info = "<span>"+start_date+"</span>" + " - " + "<span>"+end_date+"</span>" + " - " + "<span>"+vacation_title+"</span>";
        $("#next-vacation").append(next_vacation_info);        
    }    
}

function getUpcomingHolidays(user_holidays_array) {
    var filter_by_date = "DateofHoliday";
    var holidays_filtered = upcomingDatesSorted(user_holidays_array, filter_by_date);
    if (holidays_filtered.length) {
        var holiday_date = new Date(holidays_filtered[0][filter_by_date]).toLocaleDateString();
        var holiday_title = holidays_filtered[0].Title;
        $("section#upcoming-holiday.hide-section").removeClass("hide-section");
        var next_holiday_info = "<span>"+holiday_date+"</span>" + " - " + "<span>"+holiday_title+"</span>";
        $("#next-holiday").append(next_holiday_info);        
    }    
}

function upcomingDatesSorted(obj_arr, filter_by_date) {
    console.log("obj_arr", obj_arr);
    var today_date = new Date();
    var today_date_UTC = today_date.setHours(0,0,0,0);
    console.log("filter_by_date", filter_by_date);
    function checkStartDate(time_containing_item) {
//        console.log("vacation_item", vacation_item);
        var start_date = time_containing_item[filter_by_date];
//        console.log("start_date", start_date);
        start_date = new Date(start_date).getTime();
        if (start_date >= today_date_UTC) {
            return time_containing_item;
        }        
    }
    var obj_arr_filtered = obj_arr.filter(checkStartDate);
    if (obj_arr_filtered.length) {
        obj_arr_filtered.sort(function(a, b){return new Date(a[filter_by_date]).getTime() - new Date(b[filter_by_date]).getTime()}); // to do 
//        return obj_arr_filtered;
    }/* else {
        return [];
    }*/
    return obj_arr_filtered || [];
     
}

function getDurationInDays(start_date, end_date) {
    var start_date_UTC = new Date(start_date).getTime();
    var end_date_UTC = new Date(end_date).getTime();
    if (start_date_UTC==end_date_UTC) {return 1}; // if date of the start and end are same, duration is considered as one day.
    var duration_in_days = Math.ceil((end_date_UTC - start_date_UTC) / MSEC_PER_DAY) + 1; //2 days duration, for example 12-13 jun
    console.log("duration_in_days", duration_in_days);
    
    return duration_in_days;
}

function calcHolidaysInPeriod(start_date, end_date, holidays_list) {
    var holiday_date_UTC;
    var holidays_in_period = 0;
    var start_date_UTC = new Date(start_date).getTime();
    var end_date_UTC = new Date(end_date).getTime();    
    var holiday_dates_used_arr = [];
    var holidays_list = holidays_list || [];
    var holidays_list_len = holidays_list.length;
    for (var i=0; i<holidays_list_len;i++) {
        var holiday = holidays_list[i];
        holiday_date_UTC = new Date(holiday).getTime();
        if (holiday_date_UTC>=start_date_UTC && holiday_date_UTC<=end_date_UTC) {
            if (!holiday_dates_used_arr.includes(holiday_date_UTC)) { // if the date was not used                
                holidays_in_period++;
            }
            holiday_dates_used_arr.push(holiday_date_UTC);
        }
    }     
    return holidays_in_period;
}

function calcWeekendDaysInPeriod(start_date, end_date) { // if working on the weekend, have another table showing the list of those
    var weekendDays_in_period = 0;    
    
    var start_date = new Date(start_date),
        end_date = new Date(end_date), 
        isWeekend = false;

    while (start_date <= end_date) {
        var week_day = start_date.getDay();
        var is_weekendDay = (week_day === 6) || (week_day === 0); // 0 - Sunday , 6 - Saturday
        if (is_weekendDay) { 
            weekendDays_in_period++;
//            return true; 
        } // return immediately if weekend found
        start_date.setDate(start_date.getDate() + 1);
    } 
    
    console.log("weekendDays_in_period", weekendDays_in_period);
    return weekendDays_in_period;
}



function VacationsCalculation() {
//1. Data Section (var,arrays, objects)
    var vc = this;
    //Get the URI decoded URLs.
    vc.hostweburl = "";
    vc.appweburl = "";

    // resources are in URLs in the form: web_url/_layouts/15/resource
    vc.scriptbase = vc.hostweburl + "/_layouts/15/";
    vc.scriptbaseTemp = vc.scriptbase;
    
    // array of users in list 'Users' in current application
    // to send them notification about user vacation.
    vc.employeesEmails = [];
    vc.weekend_list_name = "Holidays list";
    vc.weekend_WeekendDay = "DateofHoliday";
	vc.weekend_UA = "UA";
	vc.weekend_USA = "USA";
    vc.users_list_name = "Employee";
    vc.holiday={};
    vc.holiday.holiday_list_name = "Day off list";
    vc.holiday.StartDate = "StartDate";
    vc.holiday.EndDate = "End_Date";
 
    vc.holiday_types_name = "TypeOf";

    vc.weekendList="";
    vc.sickDay = 0;
    vc.compensatoryHolidays = 0;
    vc.spentHolidays = 0;
    vc.freeHolidays = 0;
    vc.user = "";
    vc.userRole = "";
    vc.personProperties="";
    vc.userDisplayName = "";
    vc.userAppRules = "";

    vc.now = new Date();
    vc.todayDate = vc.now.format("MM.dd.yyyy HH:mm");

    vc.freeHolidaysPlus = 0;
    vc.spenHolidayMinus = 0;

    vc.context = SP.ClientContext.get_current();
    vc.userContext = vc.context.get_web().get_currentUser();
    vc.executor = "";

       

    //2. Sections Logitions (fun, metods)  
    vc.getUserName =function(_vc) {
      //  var ui= new UserInfo(_vc);
    }
    vc.getWeekend=function() {
        var gw = new ListAllHolidays(vc);
    }
    vc.getUsers = function () {
        var us = new Users(vc);
    }
    vc.getHolidays = function () {
        promise.then(function() {
            var hol = new Holidays(vc);
        });
        
    }
    vc.getAllUsers = function () {
      //  var all = new AllUsers(vc);
    }
    vc.getQueryStringParameter=function (paramToRetrieve) {
        var params =
            document.URL.split("?")[1].split("&");
        var strParams = "";
        for (var i = 0; i < params.length; i = i + 1) {
            var singleParam = params[i].split("=");
            if (singleParam[0] == paramToRetrieve)
                return singleParam[1];
        }
    }
    vc.GetSiteUrl = function() {
        return window.location.protocol + "//" + window.location.host + _spPageContextInfo.siteServerRelativeUrl;
    }

    vc.initwebUrl = function () {
        vc.hostweburl = decodeURIComponent(vc.GetSiteUrl());//decodeURIComponent(vc.getQueryStringParameter("SPHostUrl"));
        vc.appweburl = decodeURIComponent(vc.GetSiteUrl());//decodeURIComponent(vc.getQueryStringParameter("SPAppWebUrl"));
        //vc.executor = new SP.RequestExecutor(vc.appweburl);
    }

    //3. Sections Events ()
    vc.initwebUrl();
    vc.getUserName(vc);
    // Load the js files and get weekend
    $.getScript(vc.scriptbase + "SP.RequestExecutor.js", function() {
        vc.getWeekend();
        vc.getUsers();
        vc.getHolidays();
//        vc.getAllUsers();

    });
        // Get users and calculate their free days on holiday
    //$.getScript(vc.scriptbase + "SP.RequestExecutor.js", vc.getUsers);

    //$.getScript(vc.scriptbase + "SP.RequestExecutor.js", vc.getHolidays);

    //$.getScript(vc.scriptbase + "SP.RequestExecutor.js", vc.getAllUsers);
}


$(document).ready(function () {
    new VacationsCalculation();
    console.log("_spPageContextInfo.userId", _spPageContextInfo.userId);
    getUserById(loggedUser.userId);
    if (loggedUser.displayOtherStats) {
        $("#other-users-stats").removeClass("hide-section");
//        getAllUserStats();
    }
    
    
});
})(jQuery);

