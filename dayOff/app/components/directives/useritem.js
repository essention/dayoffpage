dayOff.directive("userItem", [ function () {
  return {
    restrict: 'E',
    scope: "=",
    templateUrl: "/sites/ts/SiteAssets/dayOff/app/components/directives/useritem.html"
  };
}]);