angular.module("dayOff")
.factory("GetListInfo", ['$http', function GetListInfoFactory($http) {    
    var siteUrl = _spPageContextInfo.webAbsoluteUrl;

    return {
        getListItemsByTitle: function getListItemsByTitle(title, filter_name, filter_val) {
    //        console.log("getListItemsByTitle");
            var promise;
            var url = siteUrl + "/_api/web/lists/getbytitle('"+title+"')/Items";
            if (filter_name!=undefined && filter_val!=undefined) {
                url+="?$filter="+filter_name+" eq " + filter_val;
            }  
            promise = $http({
                method: 'GET',
                url: url,
                headers: { "Accept": "application/json; odata=verbose" }
            });

            return promise;
        }


    }
}
]);



