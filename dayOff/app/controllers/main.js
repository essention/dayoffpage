"use strict";
var dayOff = angular.module('dayOff',[]);

dayOff.controller('MainController', ['$scope', '$http', '$q', 'VACATION_PER_YEAR',  function($scope, $http, $q, VACATION_PER_YEAR) {
    var MSEC_PER_DAY = 1000*60*60*24;
    var DAYS_PER_YEAR = 365;
//    var VACATION_PER_YEAR = 20;
    var holidays_array_UA = [];
    var holidays_array_US = [];
    var holidays_array_dates_UA = [];
    var holidays_array_dates_US = [];
    var loggedUser = {};
    loggedUser.userId = _spPageContextInfo.userId;
    loggedUser.vacation_list = [];
    var siteUrl = _spPageContextInfo.webAbsoluteUrl;
    $scope.loggedUser = loggedUser;
    
    var userInfo = {};
    var userList = [];
    $scope.userList = [];
    var custom_case_for_Admin = true;
    var custom_case_Lana_admin = {
        ess_group_admin_id: 18, // to do - find by name
        lana_employee_id: 32
    }
    var admin_users_ids = [ // temp, need some table list
        18,//Ruslana as Ess Group Admin
        32,//Ruslana
        11,//Chris
        15,//Alex
        49,//Oleksii
        24//Olena
    ]
    loggedUser.displayOtherStats = admin_users_ids.includes(loggedUser.userId);
    console.log("loggedUser.displayOtherStats", loggedUser.displayOtherStats);


    if (custom_case_for_Admin) {
       if (loggedUser.userId == custom_case_Lana_admin.ess_group_admin_id) {
           loggedUser.userId = custom_case_Lana_admin.lana_employee_id;
       }
    }
    getUserById(loggedUser.userId);
    
    
    function getUserContractById(userId, then_fnc) {        
        var promise = getListItemsByTitle('Employee', 'EmployeeId', userId);
        promise.then(function successCallback(response) {
            if (then_fnc!=undefined) {
                then_fnc(response);
                return then_fnc;
            }
//            return promise; 
        }, function errorCallback(response) {
            
            console.error("getUserContractById in ang", response);
        });
        return promise;
    }
    var userInformationPromise = getUserContractById(undefined, getAllEmployeesByContractList); //loggedUser.userId
    getUserContractById(loggedUser.userId); //
    
    function getAllEmployeesByContractList (response) {
        var deferred = $q.defer();
        var promise = deferred.promise;
        var contacts = response.data.d.results;
//            console.log("getUserContractById in ang", contacts);
        var filtered_contacts = contacts.filter(filterValidContracts);
//            console.log("getUserContractById in filtered_contacts", filtered_contacts);
        angular.forEach(filtered_contacts, function(employee, key) {
            userInfo = {};
            userInfo.Title = employee.Title;
            userInfo.Office = employee.Office;
            userInfo.Startworkdate = employee.Startworkdate;
            userInfo.EmployeeId = employee.EmployeeId;
            userList.push(userInfo);                
        });
        $scope.userList = userList;
        deferred.resolve();
        return promise;
    }
    
    var all_vacation_list_promise = loggedUser.displayOtherStats ? getAllItemsFromDayOffList() : getAllItemsFromDayOffList(loggedUser.userId);
    var holidays_list_promise = getListItemsFromSPList();
	
    $q.all([userInformationPromise, all_vacation_list_promise, holidays_list_promise]).then(function(reponses) {
        console.log("q.all returned", reponses, userInformationPromise);
        var vacation_list = reponses[1].data.d.results;
        getAllusersStatistics($scope.userList, vacation_list);
    });
    
    //other_employee.days_available
    function getAllusersStatistics(userList, vacationList) {
//            console.log("getAllusersStatistics userList vacationList", userList, vacationList);
        var days_available, 
            days_used,
            unpayed,
            sick_leave; 
        angular.forEach(userList, function(user) {
//            console.log("getAllusersStatistics user", user);
            days_available = calculateVacationDaysAvailable(user);
            days_used = 0;
            unpayed = 0;
            sick_leave = 0; 
            var holiday_array_dates_user = (user.Office == 'UA') ? holidays_array_dates_UA : holidays_array_dates_US;
            if (loggedUser.userId == user.EmployeeId) {  
                loggedUser.Office = user.Office;
                var holiday_array_user = (loggedUser.Office == 'UA') ? holidays_array_UA : holidays_array_US;
                getUpcomingHolidays(holiday_array_user);
            }
            
            angular.forEach(vacationList, function(vacation_item) {
                if (user.EmployeeId == vacation_item.EmployeeId) {
//                    console.log("getAllusersStatistics vacation_item", vacation_item);
                    var holidayType = vacation_item.TypeOfId;
                    var start_date = vacation_item.StartDate;
                    var end_date = vacation_item.EndDate || vacation_item.End_Date;
                    
//                    console.log("getAllusersStatistics", start_date, end_date);

                    var timePeriod_duration = getDurationInDays(start_date, end_date);
                    var weekend_days = calcWeekendDaysInPeriod(start_date, end_date);
//                    console.log("weekend_days", weekend_days);
//                    console.log("holiday_array_dates_user", holiday_array_dates_user);
                    var holidays_in_period = calcHolidaysInPeriod(start_date, end_date, holiday_array_dates_user);
//                    console.log("holidays_in_period", holidays_in_period);

                    var pureVacationTime = timePeriod_duration - weekend_days - holidays_in_period;           

                    switch (holidayType) {// 1 - vacation, 2 - compensatory leave, 3 - sick leave
                        case 1: //paid leave
                            days_used += pureVacationTime;
                            days_available -= pureVacationTime; 
                            if (loggedUser.userId == user.EmployeeId) {loggedUser.vacation_list.push(vacation_item)};
                            break;
                        case 2: //unpaid leave
                            unpayed += pureVacationTime; 
                            if (loggedUser.userId == user.EmployeeId) {loggedUser.vacation_list.push(vacation_item)};
                            break;
                        case 3: //sick days
                            sick_leave  += pureVacationTime; 
                            break;
                    }
                    
                    
                }
            });
            user.days_available = days_available;
            user.days_used = days_used;
            user.unpayed = unpayed;
            user.sick_leave = sick_leave;
            
            if (loggedUser.userId == user.EmployeeId) {
                loggedUser.days_available = days_available;
                loggedUser.days_used = days_used;
                loggedUser.unpayed = unpayed;
                loggedUser.sick_leave = sick_leave;
                
                getUpcomingVacation(loggedUser.vacation_list);
            }
            
            
        });
    }
    
   
    function calculateVacationDaysAvailable(user) {
        
        var today_UTC = new Date().getTime();
        var contract_start_UTC = new Date(user.Startworkdate).getTime();
        var contract_duration_by_now = getDurationInDays(contract_start_UTC, today_UTC);
        var days = Math.ceil((contract_duration_by_now / DAYS_PER_YEAR) * VACATION_PER_YEAR);
//        console.log("calculateVacationDaysAvailable", days);
        return days;
    }

        
    function getListItemsByTitle(title, filter_name, filter_val) {
        var promise;
        var url = siteUrl + "/_api/web/lists/getbytitle('"+title+"')/Items";
        if (filter_name!=undefined && filter_val!=undefined) {
            url+="?$filter="+filter_name+" eq " + filter_val;
        }  
        promise = $http({
            method: 'GET',
            url: url,
            headers: { "Accept": "application/json; odata=verbose" }
        });
        
        return promise;
    }
    
    function getAllItemsFromDayOffList(userId, then_fnc) {
        var promise = getListItemsByTitle('Day off list', 'EmployeeId', userId);
        promise.then(function successCallback(response) {
            
            var contacts = response.data.d.results;
            console.log("getAllItemsFromDayOffList in ang", contacts);
            if (then_fnc!=undefined) {
                then_fnc(response);
            }
            
        }, function errorCallback(response) {
            
            console.error("getAllItemsFromDayOffList in ang", response);
        });
        
        return promise;    
    }
    
    //
    
    
    //------ filters
    
    function filterValidContracts(contact) {
        return contact.contractcompleted==null;
    }
    
    function filterArrById(day_off, employee_id) {
        return day_off.EmployeeId==employee_id;
    }
    
    function filterArrByTypeOfId(employee) {
//        case 1: //paid leave
//        case 2: //unpaid leave
//        case 3: //sick days
                
    }
    
    
    function getDurationInDays(start_date, end_date) {
        var start_date_UTC = new Date(start_date).getTime();
        var end_date_UTC = new Date(end_date).getTime();
//        console.log("getDurationInDays", start_date_UTC, end_date_UTC);
        if (start_date_UTC==end_date_UTC) {return 1}; // if date of the start and end are same, duration is considered as one day.
        var duration_in_days = Math.ceil((end_date_UTC - start_date_UTC) / MSEC_PER_DAY) + 1; //2 days duration, for example 12-13 jun
//        console.log("duration_in_days", duration_in_days);

        return duration_in_days;
    }

    function calcHolidaysInPeriod(start_date, end_date, holidays_list) {
        var holiday_date_UTC;
        var holidays_in_period = 0;
        var start_date_UTC = new Date(start_date).getTime();
        var end_date_UTC = new Date(end_date).getTime();    
        var holiday_dates_used_arr = [];
        var holidays_list = holidays_list || [];
        var holidays_list_len = holidays_list.length;
        for (var i=0; i<holidays_list_len;i++) {
            var holiday = holidays_list[i];
            holiday_date_UTC = new Date(holiday).getTime();
            if (holiday_date_UTC>=start_date_UTC && holiday_date_UTC<=end_date_UTC) {
                if (!holiday_dates_used_arr.includes(holiday_date_UTC)) { // if the date was not used                
                    holidays_in_period++;
                }
                holiday_dates_used_arr.push(holiday_date_UTC);
            }
        }     
        return holidays_in_period;
    }

    function calcWeekendDaysInPeriod(start_date, end_date) { // if working on the weekend, have another table showing the list of those
        var weekendDays_in_period = 0;    

        var start_date = new Date(start_date),
            end_date = new Date(end_date), 
            isWeekend = false;

        while (start_date <= end_date) {
            var week_day = start_date.getDay();
            var is_weekendDay = (week_day === 6) || (week_day === 0); // 0 - Sunday , 6 - Saturday
            if (is_weekendDay) { 
                weekendDays_in_period++;
    //            return true; 
            } // return immediately if weekend found
            start_date.setDate(start_date.getDate() + 1);
        } 

//        console.log("weekendDays_in_period", weekendDays_in_period);
        return weekendDays_in_period;
    }
    
    function getListItemsFromSPList() {
        var promise = getListItemsByTitle('Holidays list');
        promise.then(function(response) {
//            console.log("getListItemsFromSPList", response.data.d.results);
            var holidays_list = response.data.d.results;
            var holidays_list_len = response.data.d.results.length;
            for (var i=0; i<holidays_list_len;i++) {
                var holiday = response.data.d.results[i];
    //            console.log("holiday", holiday);
                if (holiday.UA == true) {
                    holidays_array_dates_UA.push(holiday.DateofHoliday);
                    holidays_array_UA.push(holiday);
                }
                if (holiday.USA == true) {                
                    holidays_array_dates_US.push(holiday.DateofHoliday);  
                    holidays_array_US.push(holiday);          
                }
            };
            buildHolidayTable(holidays_list);

        });
        return promise;
    }
        
    function upcomingDatesSorted(obj_arr, filter_by_date) {
        var today_date = new Date();
        var today_date_UTC = today_date.setHours(0,0,0,0);
        function checkStartDate(time_containing_item) {
            var start_date = time_containing_item[filter_by_date];
            start_date = new Date(start_date).getTime();
            if (start_date >= today_date_UTC) {
                return time_containing_item;
            }        
        }
        var obj_arr_filtered = obj_arr.filter(checkStartDate);
        if (obj_arr_filtered.length) {
            obj_arr_filtered.sort(function(a, b){return new Date(a[filter_by_date]).getTime() - new Date(b[filter_by_date]).getTime()}); // to do 
    //        return obj_arr_filtered;
        }
        return obj_arr_filtered || [];
    }
    
    function getUpcomingVacation(vacations_array) {
        var vacation_filtered = upcomingDatesSorted(vacations_array, 'StartDate');
        if (vacation_filtered.length) {
            var next_vacation = {};
            next_vacation.start_date = new Date(vacation_filtered[0].StartDate).toLocaleDateString();
             var end_date_temp = vacation_filtered[0].End_Date || vacation_filtered[0].EndDate;
            next_vacation.end_date = new Date(end_date_temp).toLocaleDateString();
            next_vacation.title = vacation_filtered[0].Title;
            $scope.next_vacation = next_vacation;
     
        }    
    }

    function getUpcomingHolidays(user_holidays_array) {
        var filter_by_date = "DateofHoliday";
        var holidays_filtered = upcomingDatesSorted(user_holidays_array, filter_by_date);
        if (holidays_filtered.length) {
            var next_holiday = {};
            next_holiday.date = new Date(holidays_filtered[0][filter_by_date]).toLocaleDateString();
            next_holiday.title = holidays_filtered[0].Title;
            $scope.next_holiday = next_holiday;

        }    
    }
    
    
    function getUserById(employeeID) {
        var promise = $http({
            url: siteUrl + "/_api/web/getuserbyid(" + employeeID + ")",
            method: "GET",
            headers: { "Accept": "application/json; odata=verbose" }
        }).then(function successCallback(response) {
            console.log("response", response);
            var user = response.data.d;
            loggedUser.Title = user.Title;
            return user;
        });
    }
    
    function buildHolidayTable(results) {
        console.log("buildHolidayTable results", results);
        var weekendTable = document.getElementById("weekend"); //todo setup selector

        for (var i = 0; i < results.length; i++) {

            var row = weekendTable.insertRow();
            row.className = "tr-user";
            var cellTitle = row.insertCell();
            var cellWeekendDay = row.insertCell();
			var cellUA = row.insertCell();
			var cellUSA = row.insertCell();
			
			cellTitle.innerHTML = results[i].Title;

            var weekTempDay = new Date(results[i].DateofHoliday);
            var temp = weekTempDay.format('MMM dd yyyy');

            cellWeekendDay.innerHTML = temp;
			
			if(results[i].UA)
				cellUA.innerHTML = results[i].UA;
			if(results[i].USA)
				cellUSA.innerHTML = results[i].USA;
            
        }
    }

    
    
}]);